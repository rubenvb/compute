/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Ruben Van Boxem
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 **/

/*
 * aligned_allocator.h++
 * Provides aligned arrays so that CPU intrinsics such as SSE can be used directly.
 */

#ifndef COMPUTE_ALIGNED_ALLOCATOR_H
#define COMPUTE_ALIGNED_ALLOCATOR_H

#ifdef _WIN32
#include <malloc.h>
#elif __unix__
#include <stdlib.h>
#endif

namespace compute
{
  template<typename T, std::size_t alignment = alignof(std::max_align_t)>
  struct aligned_allocator
  {
    using pointer = T*;
    using const_pointer = const T*;
    using reference = T&;
    using const_reference = const T&;
    using value_type = T;
    using size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    pointer address(reference r) const
    {
      return &r;
    }

    const_pointer address(const_reference s) const
    {
      return &s;
    }

    size_type max_size() const
    {
      return size_type(-1) / sizeof(T);
    }

    // The following must be the same for all allocators.
    template<typename U>
    struct rebind
    {
      using other = aligned_allocator<U, alignment>;
    };

    bool operator!=(const aligned_allocator& other) const
    {
      return !(*this == other);
    }

    void construct(pointer const p, const_reference t) const
    {
      void* const pv = static_cast<void*>(p);

      new (pv) T(t);
    }

    void destroy(pointer const p) const; // Defined below

    bool operator==(const aligned_allocator& other) const
    {
      return true;
    }

    pointer allocate(const size_type n) const
    {
      if(n == 0)
        return nullptr;

      if (n > max_size())
         throw std::length_error("integer overflow in aligned_allocator.");

  #ifdef _WIN32
      void* const pv = ::_aligned_malloc(n*sizeof(value_type), alignment);
  #elif __unix__
      void* pv;
      ::posix_memalign(&pv, alignment, n*sizeof(value_type));
  #else
  #error no aligned memory allocation defined on this platform
  #endif

      if(pv == nullptr)
        throw std::bad_alloc();

      return static_cast<pointer>(pv);
    }

    void deallocate(pointer const p, const size_type n) const
    {
  #ifdef _WIN32
      ::_aligned_free(p);
  #elif __unix__
      ::free(p);
  #endif
    }

    // The following will be the same for all allocators that ignore hints.
    template<typename U> T* allocate(const size_t n, const U* /* const hint */) const
    {
      return allocate(n);
    }

    aligned_allocator& operator=(const aligned_allocator&) = delete;
  };

  template<typename T, std::size_t alignment> void aligned_allocator<T, alignment>::destroy(T* const p) const
  {
    p->~T();
  }

} // namespace compute

#endif // COMPUTE_ALIGNED_ALLOCATOR_H
