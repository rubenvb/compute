/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Ruben Van Boxem
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 **/

/*
 * memory.h++
 * types and interface for buffer allocation and device-host copy:
 *  - Arithmetic types
 *  - Arrays
 *  - Copy
 */

#ifndef COMPUTE_MEMORY_H
#define COMPUTE_MEMORY_H

#include "aligned_allocator.h++"
#include "complex.h++"

namespace compute
{
  namespace memory_implementation
  {
    template<typename, device, backend>
    struct memory;

    template<typename T, device d = device::CPU, backend b = backend::CPU>
    struct memory
    {
      static_assert(std::is_floating_point<T>::value, "compute::memory can only be used for floating point types.");
      static_assert(d == device::CPU,  "compute::memory has no specialization for selected device.");
      static_assert(b == backend::CPU , "compute::memory has no specialization for selected backend.");

      using real_type    = T;
      using complex_type = std::complex<T>;
      using real_vector    = std::vector<real_type,    aligned_allocator<T, 16>>;
      using complex_vector = std::vector<complex_type, aligned_allocator<complex_type, 16>>;
    };

    template<>
    struct memory<double, device::CPU, backend::VexCL>
    {
      using real_type    = double;
      using complex_type = cl_double2;

      using real_vector    = vex::vector<real_type>;
      using complex_vector = vex::vector<complex_type>;
    };

    template<>
    struct memory<float, device::CPU, backend::VexCL>
    {
      using real_type    = float;
      using complex_type = cl_float2;

      using real_vector    = vex::vector<real_type>;
      using complex_vector = vex::vector<cl_float2>;
    };
  } // namespace detail

  // General interface type
  template<typename T, device d, backend b>
  struct memory : memory_implementation::memory<T, d, b> {};

  // CPU aliases for FFTW, Eigen, Armadillo,
  template<typename T>
  struct memory<T, device::CPU, backend::FFTW> : memory_implementation::memory<T, device::CPU, backend::CPU> {};

  template<typename T>
  struct memory<T, device::CPU, backend::Eigen> : memory_implementation::memory<T, device::CPU, backend::CPU> {};

  template<typename T>
  struct memory<T, device::CPU, backend::Armadillo> : memory_implementation::memory<T, device::CPU, backend::CPU> {};

} // namespace compute

#endif // COMPUTE_MEMORY_H
