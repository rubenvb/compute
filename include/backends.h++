/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Ruben Van Boxem
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 **/

/*
 * backends.h++
 * Available backends.
 */

#ifndef COMPUTE_BACKENDS_H
#define COMPUTE_BACKENDS_H

namespace compute
{
  enum class backend
  {
    CPU,
    None = CPU, // CPU only (pure non-3rd-party C++, haha, let's implement all the things!)
    OpenCL,     // Uses clMath where possible
    CUDA,       // NVIDIA GPU only, provides cuFFT/cuBLAS/...
    VexCL,      // OpenCL, uses expression templates
    VexCL_CUDA, // CUDA
    AMP,        // Microsoft specification for simple device parallellism

    // incomplete backends which can only be used for a subset of operations
    FFTW,       // CPU FFT only, possibly multithreaded
    Eigen,      // CPU matrix only, possible multithreaded
    Armadillo,  // CPU matrix+fft, possibly multithreaded

    // CUDA libraries
    cuFFT,
    cuBLAS,
    Thrust,

    // OpenCL libraries
    clFFT,
    clBLAS,
    Bolt,
    ViennaCL
  };
}

#endif // COMPUTE_BACKENDS_H

