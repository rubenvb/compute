/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Ruben Van Boxem
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 **/

/*
 * compute.h++
 * Computation backend abstraction layer, templatized.
 * Abstractions are provided for:
 *  - FFT: FFTW, VexCL, clFFT, cuFFT
 *  - General computations: CPU, VexCL
 *  - ...
 */

#ifndef COMPUTE_H
#define COMPUTE_H

#include <stdexcept>
#include <vector>


#include "compute_context.h++"
#include "memory.h++"
#include "fft.h++"
#include "matrix.h++"

// Public interface
namespace compute
{

  template<typename T = double,
           device   compute_device = device::CPU,
           backend  general_backend = backend::CPU,
           backend  fft_backend = backend::CPU,
           backend  matrix_backend = backend::CPU>
  struct calculator
  {
    // components
    using memory = ::compute::memory<T, compute_device, general_backend>;
    using FFT    = ::compute::FFT<T, compute_device, fft_backend>;
    using matrix = ::compute::matrix<T, compute_device, matrix_backend>;

    // import typenames and functions for public use
    using vector        = typename memory::vector;
    using complex_type  = typename memory::complex_type;
    using fft_plan_type = typename FFT::plan_type;
  };

  template<typename T>
  using host = memory<T, device::CPU, backend::CPU>;

} // namespace compute

#endif // COMPUTE_H
