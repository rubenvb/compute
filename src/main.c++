/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Ruben Van Boxem
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 **/

#include <chrono>
#include <iostream>
#include <string>

using namespace std::chrono;

#include "compute.h++"
#include "complex.h++"

using compute::device;
using compute::backend;

template<typename T, device d, backend b>
void benchmark(std::size_t N);

int main(int argc, char* argv[])
{
  std::size_t N = 1024;
  if(argc > 1)
    N = std::stoull(argv[1]);

  // warmup run
  benchmark<float, device::CPU, backend::FFTW>(16);

  benchmark<double, device::CPU, backend::FFTW>(N);
  benchmark<float, device::CPU, backend::FFTW>(N);

  //benchmark<double, device::CPU, backend::VexCL>(N);
  //benchmark<double, device::CPU, backend::VexCL>(N);

  //benchmark<float, device::GPU, backend::VexCL>(N);
  //benchmark<double, device::GPU, backend::VexCL>(N);

  //benchmark<float, device::CPU, backend::OpenCL>(N);
  //benchmark<double, device::CPU, backend::OpenCL>(N);

  //benchmark<float, device::GPU, backend::OpenCL>(N);
  //benchmark<double, device::GPU, backend::OpenCL>(N);
#ifdef HAVE_CUDA
  //benchmark<float, device::GPU, backend::VexCL_CUDA>(N);
  //benchmark<double, device::GPU, backend::VexCL_CUDA>(N);

  //benchmark<float, device::GPU, backend::CUDA>(N);
  //benchmark<double, device::GPU, backend::CUDA>(N);
#endif
}

template<typename T, device d, backend b>
void benchmark(std::size_t N)
{
  using memory = compute::memory<T, d, b>;
  using FFT = compute::FFT<T, d, b>;
  using random = compute::random<T, d, b>;

  auto tic = high_resolution_clock::now();
  auto begin = tic;
  compute::initialize<T, d, b>(8);
  auto toc = high_resolution_clock::now();

  std::cout << "-> Initialization time: " << duration<double, std::milli>(toc-tic).count() << " ms.\n";

  tic = high_resolution_clock::now();
  typename memory::complex_vector original(N*N);
  typename memory::complex_vector transform(N*N);
  typename memory::complex_vector result(N*N);
  toc = high_resolution_clock::now();

  std::cout << "-> Allocation time:     " << duration<double, std::milli>(toc-tic).count() << " ms.\n";

  tic = high_resolution_clock::now();
  auto plan_forward = FFT::plan({N, N}, original, transform, +1);
  auto plan_backward = FFT::plan({N, N}, transform, result, -1);
  toc = high_resolution_clock::now();

  std::cout << "-> FFT plan time:       " << duration<double, std::milli>(toc-tic).count() << " ms.\n";

  tic = high_resolution_clock::now();

  auto rng = random::generator(rng::mersenne_twister);

  fill(original, &rng)
  /*for(std::size_t i = 0; i < original.size(); ++i)
  {
    compute::real(original[i]) = dist(eng);
    compute::imag(original[i]) = dist(eng);
  }*/
  toc = high_resolution_clock::now();

  std::cout << "-> Generation time:     " << duration<double, std::milli>(toc-tic).count() << " ms.\n";

  tic = high_resolution_clock::now();
  FFT::execute(plan_forward, original, transform);
  FFT::execute(plan_backward, transform, result);
  toc = high_resolution_clock::now();

  std::cout << "-> FFT/IFFT time:       " << duration<double, std::milli>(toc-tic).count() << " ms.\n";

  // FFTW is asymmetrically normalized
  for(std::size_t i = 0; i<original.size(); ++i)
  {
    compute::real(result[i]) = compute::real(result[i]) / original.size();
    compute::imag(result[i]) = compute::imag(result[i]) / original.size();
  }
  toc = high_resolution_clock::now();

  std::cout << "-> Normalization time:  " << duration<double, std::milli>(toc-tic).count() << " ms.\n";

  tic = high_resolution_clock::now();
  double error = 0.;
  for(std::size_t i = 0; i< original.size(); ++i)
  {
    error += abs(original[i] - result[i]);
  }
  toc = high_resolution_clock::now();

  std::cout << "-> Error calc time:     " << duration<double, std::milli>(toc-begin).count() << ".\n";

  std::cout << "-> Error: " << error << ".\n";

  std::cout << "> TOTAL TIME: " << duration<double, std::milli>(toc-begin).count() << " ms.\n";
}
