/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Ruben Van Boxem
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 **/

/*
 * compute_context.h++
 * Description/handle to a device+backend, mostly so that all functions can access
 * the necessary internals.
 *
 * TODO: allow for mixed device contexts and multiple devices (OpenCL and MPI)
 * TODO: runtime plugability so that the device can be chosen at runtime
 */

#ifndef COMPUTE_COMPUTE_CONTEXT_H
#define COMPUTE_COMPUTE_CONTEXT_H

#include <thread>

#ifdef HAVE_FFTW
#include <fftw3.h>
#endif

#ifdef HAVE_VEXCL
#include <vexcl/vexcl.hpp>
#endif

#include "backends.h++"
#include "devices.h++"

namespace compute
{
  // default
  template<typename T, device d = device::CPU, backend b = backend::CPU>
  struct compute_context
  {
    static_assert(std::is_floating_point<T>::value, "compute::compute_context can only be used for floating point types.");
    static_assert(d == device::CPU,  "compute_context has no specialization for selected device type.");
    static_assert(b == backend::CPU, "compute_context has no specialization for selected backend.");

    compute_context(std::size_t number_of_threads = 1) : number_of_threads(number_of_threads) {}

    std::size_t number_of_threads;
  };

  // Initialization: create singleton context with certain parameters
  template<typename T, device d = device::CPU, backend b = backend::CPU>
  const compute_context<T, d, b>& initialize(std::size_t number_of_threads = std::thread::hardware_concurrency(),
                                             std::size_t number_of_devices = 1)
  {
    static compute_context<T, d, b> context(number_of_threads, number_of_devices);
    return context;
  }
  // Alias to return the relevant context where needed
  template<typename T, device d, backend b>
  const compute_context<T, d, b>& context()
  {
    return initialize<T, d, b>();
  }

  template<>
  struct compute_context<double, device::CPU, backend::FFTW>
  {
    compute_context(std::size_t number_of_threads = 1,
                    std::size_t /*number_of_devices*/ = 1)
      : number_of_threads(number_of_threads)
    {
      if(fftw_init_threads() == 0)
        throw std::runtime_error("Call to fftwf_init_threads failed.");
      fftw_plan_with_nthreads(number_of_threads);
    }
    ~compute_context()
    {
      fftw_cleanup_threads();
    }

    std::size_t number_of_threads;
  };

  template<>
  struct compute_context<float, device::CPU, backend::FFTW>
  {
    compute_context(std::size_t number_of_threads = 1,
                    std::size_t /*number_of_devices*/ = 1)
      : number_of_threads(number_of_threads)
    {
      if(fftwf_init_threads() == 0)
        throw std::runtime_error("Call to fftwf_init_threads failed.");
      fftwf_plan_with_nthreads(number_of_threads);
    }
    ~compute_context()
    {
      fftwf_cleanup_threads();
    }

    std::size_t number_of_threads;
  };

  template<>
  struct compute_context<double, device::CPU, backend::VexCL>
  {
    compute_context(std::size_t number_of_threads = 1,
                    std::size_t number_of_devices = 1)
      : number_of_threads(number_of_threads),
        context(vex::Filter::CPU
                && vex::Filter::DoublePrecision
                && vex::Filter::Count(number_of_devices))
    {
      if(!context)
        throw std::runtime_error("Cannot initialize double precision VexCL CPU context.");
    }

    std::size_t number_of_threads;

    vex::Context context;
  };

  template<>
  struct compute_context<float, device::CPU, backend::VexCL>
  {
    compute_context(std::size_t number_of_threads = 1,
                    std::size_t number_of_devices = 1)
      : number_of_threads(number_of_threads),
        context(vex::Filter::CPU
                && vex::Filter::Count(number_of_devices))
    {
      if(!context)
        throw std::runtime_error("Cannot initialize single precision VexCL CPU context.");
    }

    std::size_t number_of_threads;

    vex::Context context;
  };

  template<>
  struct compute_context<double, device::GPU, backend::VexCL>
  {
    compute_context(std::size_t number_of_threads = 1,
                    std::size_t number_of_devices = 1)
      : number_of_threads(number_of_threads),
        context(vex::Filter::GPU
                && vex::Filter::DoublePrecision
                && vex::Filter::Count(number_of_devices))
    {
      if(!context)
        throw std::runtime_error("Cannot initialize double precision VexCL GPU context.");
    }

    std::size_t number_of_threads;

    vex::Context context;
  };

  template<>
  struct compute_context<float, device::GPU, backend::VexCL>
  {
    compute_context(std::size_t number_of_threads = 1,
                    std::size_t number_of_devices = 1)
      : number_of_threads(number_of_threads),
        context(vex::Filter::GPU
                && vex::Filter::Count(number_of_devices))
    {
      if(!context)
        throw std::runtime_error("Cannt initialize single precision VexCL GPU context");
    }

    std::size_t number_of_threads;

    vex::Context context;
  };

} // namespace compute

#endif // COMPUTE_COMPUTE_CONTEXT_H
