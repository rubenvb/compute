# Compute

This library provides a template-based abstraction interface of various compute devices and backend libraries.

# Status

Early stage of development.
Everything is subject to change and all help is welcome!

# Goal

The primary goal is to provide an easy way into general compute operations in C++.
It should be as easy as replacing the backend and device identifiers to switch which is used.
Currently this is done with template parameters, but this really should be a runtime choice.
This can be accomplished either through a factory interface such as the one described in implementing an efficient FFT
with C++ templates LINK TO ARTICLE using the Loki library.

# Features

The most crucial features will be implemented:
 - Memory management: also using different backends, and allow interop where possible.
 - array operations (cfr. vex::vector)
 - FFT
 - Matrix operations
 - ...

# Future plans

Once the basic infrastructure is in place, several things need to be done to make this library better than others:
 - propagate the expression template math from e.g. Eigen, Armadillo, and VexCL to the library interface.
 - ...
