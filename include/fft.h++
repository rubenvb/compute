/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Ruben Van Boxem
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 **/

/*
 * fft.h++
 * FFT API wrapper.
 */

#ifndef COMPUTE_FFT_H
#define COMPUTE_FFT_H

#include "backends.h++"
#include "devices.h++"
#include "memory.h++"

namespace compute
{
  namespace fft_implementation
  {
    template<typename T, device d = device::CPU, backend b = backend::CPU>
    struct FFT
    {
      static_assert(std::is_floating_point<T>::value, "compute::FFT can only be used for floating point types.");
      static_assert(d == device::CPU,  "FFT has no specialization for selected device.");
      static_assert(b == backend::CPU, "FFT has no specialization for selected backend.");

      using memory_type    = memory<T, d, b>;
      using complex_type   = typename memory_type::complex_type;
      using complex_vector = typename memory_type::complex_vector;

      using plan_type = int; // dummy - nothing to keep track of!

      static plan_type plan(const std::vector<std::size_t>& dimensions,
                            complex_vector& input,
                            complex_vector& output,
                            int sign = +1)
      {
        return 0;
      }
    };

    template<>
    struct FFT<double, device::CPU, backend::FFTW>
    {
      using memory_type = memory<double, device::CPU, backend::FFTW>;
      using complex_type   = typename memory_type::complex_type;
      using complex_vector = typename memory_type::complex_vector;

      using plan_type = std::unique_ptr<std::remove_pointer<fftw_plan>::type, decltype(&fftw_destroy_plan)>;

      static plan_type plan(const std::vector<std::size_t>& dimensions,
                            complex_vector& input,
                            complex_vector& output,
                            int sign = +1)
      {
        const std::vector<int> dims(begin(dimensions), end(dimensions));
        fftw_plan result = fftw_plan_dft(dims.size(), dims.data(),
                                         reinterpret_cast<fftw_complex*>(input.data()),
                                         reinterpret_cast<fftw_complex*>(output.data()),
                                         sign > 0 ? FFTW_FORWARD : FFTW_BACKWARD,
                                         FFTW_MEASURE);
        if(result == nullptr)
          throw std::runtime_error("fftw_plan_dft call failed.");
        return { result, &fftw_destroy_plan };
      }
      static void execute(const plan_type& fft_plan,
                          const complex_vector&,
                          complex_vector&)
      {
        fftw_execute(fft_plan.get());
      }
    };

    template<>
    struct FFT<float, device::CPU, backend::FFTW>
    {
      using memory_type    = memory<float, device::CPU, backend::FFTW>;
      using complex_type   = fftwf_complex;
      using complex_vector = memory_type::complex_vector;

      using plan_type = std::unique_ptr<std::remove_pointer<fftwf_plan>::type, decltype(&fftwf_destroy_plan)>;

      static plan_type plan(const std::vector<std::size_t>& dimensions,
                            complex_vector& input,
                            complex_vector& output,
                            int sign = +1)
      {
        const std::vector<int> dims(begin(dimensions), end(dimensions));
        return { fftwf_plan_dft(dimensions.size(), dims.data(),
                                reinterpret_cast<complex_type*>(input.data()),
                                reinterpret_cast<complex_type*>(output.data()),
                                sign > 0 ? FFTW_FORWARD : FFTW_BACKWARD,
                                FFTW_MEASURE),
                 &fftwf_destroy_plan };
      }
      static void execute(const plan_type& fft_plan,
                          const complex_vector&,
                          complex_vector&)
      {
        fftwf_execute(fft_plan.get());
      }
    };

    template<typename T, device d>
    struct FFT<T, d, backend::VexCL>
    {
      using memory_type    = memory<T, d, backend::VexCL>;
      using complex_type   = typename memory_type::complex_type;
      using complex_vector = typename memory_type::complex_vector;

      using plan_type = vex::FFT<complex_type>;

      static plan_type plan(const std::vector<std::size_t>& dimensions,
                            complex_vector& input,
                            complex_vector& output,
                            int sign = +1)
      {
        return vex::FFT<complex_type>(context<T, d, backend::VexCL>().context, dimensions);
      }
      static void execute(const plan_type& fft_plan,
                          const complex_vector& input,
                          complex_vector& output)
      {
        fft_plan(input, output);
      }
    };

  } // namespace detail

  // General interface type
  template<typename T, device d, backend b>
  struct FFT : fft_implementation::FFT<T,d,b> {};

  // Aliases
  template<typename T>
  struct FFT<T, device::GPU, backend::cuFFT> : fft_implementation::FFT<T, device::GPU, backend::CUDA> {};

  template<typename T, device d>
  struct FFT<T, d, backend::clFFT> : fft_implementation::FFT<T, d, backend::OpenCL> {};

} // namespace compute

#endif // COMPUTE_FFT_H
