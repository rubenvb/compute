/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Ruben Van Boxem
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 **/

/*
 * complex.h++
 * Complex value type interface and wrapper.
 */

#ifndef COMPUTE_RANDOM_H
#define COMPUTE_RANDOM_H

#include "devices.h++"
#include "backends.h++"
#include "memory.h++"

namespace compute
{
  enum class rng
  {
    linear_congruential,
    mersenne_twister,
    subtract_with_carry,
    discard_block,
    shuffle_order
  };

  template<typename T, device d = device::CPU, backend b = backend::CPU>
  struct random
  {
    static_assert(std::is_floating_point<T>::value, "compute::random can only be used for floating point types.");
    static_assert(d == device::CPU,  "compute::random has no specialization for selected device.");
    static_assert(b == backend::CPU, "compute::random has no specialization for selected backend.");

    using memory_type = memory<T, d, b>;
    using real_vector = memory_type::real_vector;
    using complex_vector = memory_type::complex_vector;

    template<typename Vector, rng type = rng::mersenne_twister, typename... ArgTypes>
    static Vector random_vector(std::size_t N,
                                ArgTypes...)
    {
      static std::uniform_real_distribution<T> dist(0., 1.);
      Vector(N);
      switch(type)
      {
        case rng::linear_congruential:
          std::minstd_rand eng;

      }

      std::mt19937_64 eng;

      Vector::value_type
      for(auto& element : )
    }
  private:
    template<typename Vector, typename Engine>
    Vector generate(Vector& v,
                    const std::uniform_distribution<Vector::value_type>& dist)
    {


    }

  };

} // namespace compute

#endif // COMPUTE_RANDOM_H
