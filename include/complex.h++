/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Ruben Van Boxem
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 **/

/*
 * complex.h++
 * Complex value type interface and wrapper.
 */

#ifndef COMPUTE_COMPLEX_H
#define COMPUTE_COMPLEX_H

namespace compute
{
  namespace detail
  {
    template<typename Complex>
    using real_type = typename std::conditional<sizeof(Complex) == 2*sizeof(double),
                                                double,
                                                float>::type;
  }
  template<typename Complex>
  typename detail::real_type<Complex>& real(Complex &c)
  {
    return reinterpret_cast<detail::real_type<Complex> (&)[2]>(c)[0];
  }
  template<typename Complex>
  const typename detail::real_type<Complex>& real(const Complex &c)
  {
    return reinterpret_cast<const detail::real_type<Complex> (&)[2]>(c)[0];
  }

  template<typename Complex>
  typename detail::real_type<Complex>& imag(Complex &c)
  {
    return reinterpret_cast<detail::real_type<Complex> (&)[2]>(c)[1];
  }
  template<typename Complex>
  const typename detail::real_type<Complex>& imag(const Complex &c)
  {
    return reinterpret_cast<const detail::real_type<Complex> (&)[2]>(c)[1];
  }

  // Math functions
  template<typename Complex>
  Complex operator+(const Complex& c1, const Complex& c2)
  {
    return Complex{real(c1) + real(c2), imag(c1) + imag(c2)};
  }
  template<typename Complex>
  Complex operator-(const Complex& c1, const Complex& c2)
  {
    return Complex{real(c1) - real(c2), imag(c1) - imag(c2)};
  }
  template<typename Complex>
  typename Complex::real_type abs(const Complex& c)
  {
    return sqrt(real(c)*real(c) + imag(c)*imag(c));
  }
}

#endif // COMPUTE_COMPLEX_H
